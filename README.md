# PolyBases.jl
A [Julia](https://julialang.org/) package for constructing combinatorial bases of highest weight representations of Simple Lie algebras and their submodules.

## Installation

In `Julia`:
```
using Pkg; pkg"add https://gitlab.com/johannesflake/polybases.jl.git ; precompile"
```
If you have not already installed Julia, you can install by following step 1 and step 2 on the [OSCAR installation page](https://oscar.computeralgebra.de/install/).

## Project status

The package is being developed and tested using Julia 1.3.1 or later. You will also need a current version of Polymake and the Julia package Gap.jl. 

You do not need the full OSCAR installation. We however recommend it since having it implies all the above are also installed.

Currently you will also need SAGE but this might change at some point.

## Use

In `Julia`:
```
using PolyBases
PolyBases.shape([6,2,5,3,1,4])
```

Type `PolyBases.` and hit the tab key to see your options.

## Questions

If you encounter any problems, please open an [issue](https://gitlab.com/johannesflake/polybases.jl/-/issues)

## Docs

[https://johannesflake.gitlab.io/polybases.jl/](https://johannesflake.gitlab.io/polybases.jl/)
