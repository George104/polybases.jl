using PolyBases
function do_test(exp)
  if eval(exp) === true println(".")
  else println("FAIL: ", string(exp))
  end
end

######################### begin tests ###############################

do_test(: (1<2) )

expected_result = Dict( (1,2)=>1, (2,1)=>2 )
do_test(: (PolyBases.dimensions_all_w([1,0]) == expected_result) )



# TODO: more tests


