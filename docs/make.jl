using Documenter
import PolyBases # together with Revise, this forces reload 

makedocs(sitename="PolyBases.jl",repo="https://gitlab.com/johannesflake/polybases.jl/blob/{commit}{path}#L{line}")
