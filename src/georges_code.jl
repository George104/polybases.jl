from itertools import product, combinations


def filterCol(n, muj, col):
  # condition (2)
  for ii in combinations(range(muj), 2):
    if col[ii[0]] != ii[0] and col[ii[0]] <= col[ii[1]]: return False

  for (i, Tij) in enumerate(col):

    # condition (1)
    if Tij != i and Tij <= muj-1: return False

    # condition (4)
    if Tij == i and i+1 < muj:
      for otherTij in col[i+1:]:
        if otherTij == 2*n-1-Tij: return False

  return True


def filterPbwC(n, t):
  for (j,col) in enumerate(t):
    for (i, Tij) in enumerate(col):
      # condition (3)  checks across cols 
      if j>0 and Tij > max(t[j-1][i:]): return False
      
  return True


def allTableaux(n, mu):
  res = [ [] ]
  cols = []
  for (j,muj) in enumerate(mu):
    if j == 0 or muj != mu[j-1]: # use already computed cols, if possible
      cols = [c for c in product(range(2*n), repeat=muj) if filterCol(n,muj,c)] 
    res = [ t + [c] for t in res for c in cols ]
    res = [ t for t in res if filterPbwC(n, t) ]
  return res

def printTableaux(result):
  for t in result:
    for i in range(len(t[0])):
      print "  ".join((str(col[i]) if i < len(col) else "") for col in t)
    print " " 

  
mu = (3,3,3)
n = 4
result = allTableaux(n, mu)
if len(result) < 150: 
    printTableaux(result)
print len(result)
